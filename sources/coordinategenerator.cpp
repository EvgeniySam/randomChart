#include <QRandomGenerator>
#include <QtConcurrent/QtConcurrent>
#include "coordinategenerator.h"
#include <QDebug>

bool CoordinateGenerator::STOP_THREAD = false;
bool CoordinateGenerator::PAUSE_THREAD = true;

CoordinateGenerator::CoordinateGenerator(QObject *parent) : QObject(parent)
{
    m_coordinateArray.reserve(HIGHEST);
    auto future = QtConcurrent::run(this, &CoordinateGenerator::calculationCoordinate);
}

void CoordinateGenerator::startCalculationCoordinate()
{
    PAUSE_THREAD = false;
    m_cv.notify_one();
}

void CoordinateGenerator::stopCalculationCoordinate()
{
    STOP_THREAD = true;
}

void CoordinateGenerator::pauseCalculationCoordinate()
{
    PAUSE_THREAD = true;
}

void CoordinateGenerator::calculationCoordinate()
{
    while (!STOP_THREAD) {
        {
            std::unique_lock<std::mutex> lockArray(m_mutexArray);
            m_cv.wait(lockArray, [](){return !PAUSE_THREAD;});
            m_coordinateArray.clear();
            for(int i = 0; i < HIGHEST; ++i){
                m_coordinateArray.push_back(QPoint(QRandomGenerator::global()->bounded(HIGHEST),
                                                   QRandomGenerator::global()->bounded(HIGHEST)));
            }
            lockArray.unlock();
        }
        emit coordinateArrayChanged();
        this->thread()->sleep(1);
    }
    m_coordinateArray.clear();
}

QVector<QPoint> CoordinateGenerator::coordinateArray()
{
    std::lock_guard<std::mutex> lockArray(m_mutexArray);
    return m_coordinateArray;
}
