#include <QApplication>
#include <QQmlApplicationEngine>
#include "coordinategenerator.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/resources/main.qml"));
    qmlRegisterType<CoordinateGenerator>("CoordinateGenerator",1,0,"CoordinateGenerator");
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);
    return app.exec();
}
