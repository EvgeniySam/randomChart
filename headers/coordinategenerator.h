#ifndef COORDINATEGENERATOR_H
#define COORDINATEGENERATOR_H
#include <QObject>
#include <QPoint>
#include <mutex>
#include <condition_variable>

#define HIGHEST 20

class CoordinateGenerator: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVector<QPoint> coordinateArray READ coordinateArray NOTIFY coordinateArrayChanged)
public:
    explicit CoordinateGenerator(QObject *parent = nullptr);
    Q_INVOKABLE void startCalculationCoordinate();
    Q_INVOKABLE void stopCalculationCoordinate();
    Q_INVOKABLE void pauseCalculationCoordinate();
    QVector<QPoint> coordinateArray();
    enum Command{
        Play, Pause, Stop
    };
    Q_ENUM(Command);
signals:
    void coordinateArrayChanged();
private:
    void calculationCoordinate();
    QVector<QPoint> m_coordinateArray;
    std::mutex m_mutexArray;
    std::condition_variable m_cv;
    static bool STOP_THREAD;
    static bool PAUSE_THREAD;

};

#endif // COORDINATEGENERATOR_H
