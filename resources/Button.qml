import QtQuick 2.0

Rectangle {
    id: control
    property alias imgSource: img.source
    signal click()
    radius: 5
    border.color: "black"
    Image {
        id: img
        anchors.fill: parent
        anchors.topMargin: 10
        anchors.bottomMargin: 10
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        fillMode: Image.PreserveAspectFit
    }
    MouseArea{
        anchors.fill: parent
        onClicked: {
            click()
        }
    }
}
