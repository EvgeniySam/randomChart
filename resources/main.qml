import QtQuick 2.12
import QtQuick.Window 2.12
import QtCharts 2.0
import CoordinateGenerator 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: "Точечная диаграмма"
    CoordinateGenerator{
        id: coordinateGenerator
        onCoordinateArrayChanged: {
            series2.clear()
            for(var it = 0; it <coordinateArray.length; ++it){
                series2.append(coordinateArray[it].x, coordinateArray[it].y)
            }
        }
    }

    ChartView {
        id: chartView
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: buttonRow.top
        legend.visible: false
        antialiasing: true

        ValueAxis {
            id: axisX
            min: 0
            max: 20
            tickCount: 5
        }

        ValueAxis {
            id: axisY
            min: 0
            max: 20
        }

        ScatterSeries {
            id: series2
            axisX: axisX
            axisY: axisY

        }
    }

    Row{
        id: buttonRow
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        height: 60
        spacing: 10
        Repeater{
            model: ListModel{
                ListElement {
                    url: "qrc:/img/play.png"
                    command: CoordinateGenerator.Play
                }
                ListElement {
                    url: "qrc:/img/pause.png"
                    command: CoordinateGenerator.Pause
                }
                ListElement {
                    url: "qrc:/img/stop.png"
                    command: CoordinateGenerator.Stop
                }
            }

            delegate: Button{
                height: parent.height
                width: height
                imgSource: url
                onClick: {
                    switch(command){
                       case CoordinateGenerator.Play:
                           coordinateGenerator.startCalculationCoordinate()
                           break
                       case CoordinateGenerator.Pause:
                           coordinateGenerator.pauseCalculationCoordinate()
                           break
                       case CoordinateGenerator.Stop:
                           coordinateGenerator.stopCalculationCoordinate()
                           series2.clear()
                           break
                    }
                }
            }
        }
    }
}
